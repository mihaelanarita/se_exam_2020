public class S2 {
    public static void main(String[] args) {
        SeThread1 t1 = new SeThread1();
        SeThread2 t2 = new SeThread2();
        t1.start();
        t2.start();
    }
}
class SeThread1 extends Thread {
    @Override
    public void run() {
        for(int i=0; i<6; i++){
            synchronized (this) {
                System.out.println("SeThread1 - " + java.time.LocalTime.now());
                try {
                    sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
class SeThread2 extends Thread {
    @Override
    public void run() {
        for(int i=0; i<6; i++){
            synchronized (this) {
                System.out.println("SeThread2 - " + java.time.LocalTime.now());
                try {
                    sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
