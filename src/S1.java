public class S1 {
    public static void main(String[] args) {
        H h= new H();
        E e = new E(h);
    }
}

class G{
    public void i (){

    }
}
class B extends G{
    E e;
    private long t;
    public void x(){
    }
}
class D {
    D(B b){
        b.x();
    }
}
class E implements I{
    F f;
    H h;
    E(H h){
        this.f = new F(); // composition
        this.h = h; // aggregation
    }
    public void metG (int i){
    }
}
class H {
}
class F {
    public void metA(){

    }
}
interface I{
}